var namespace_m_w_h =
[
    [ "API", null, [
      [ "Monitors", "struct_m_w_h_1_1_a_p_i_1_1_monitors.html", "struct_m_w_h_1_1_a_p_i_1_1_monitors" ]
    ] ],
    [ "APIError", "struct_m_w_h_1_1_a_p_i_error.html", "struct_m_w_h_1_1_a_p_i_error" ],
    [ "Monitor", "struct_m_w_h_1_1_monitor.html", "struct_m_w_h_1_1_monitor" ],
    [ "MonitorError", "struct_m_w_h_1_1_monitor_error.html", "struct_m_w_h_1_1_monitor_error" ],
    [ "MWHError", "struct_m_w_h_1_1_m_w_h_error.html", "struct_m_w_h_1_1_m_w_h_error" ],
    [ "Window", "class_m_w_h_1_1_window.html", "class_m_w_h_1_1_window" ],
    [ "WindowError", "struct_m_w_h_1_1_window_error.html", "struct_m_w_h_1_1_window_error" ]
];