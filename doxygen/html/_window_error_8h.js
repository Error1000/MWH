var _window_error_8h =
[
    [ "WindowError", "struct_m_w_h_1_1_window_error.html", "struct_m_w_h_1_1_window_error" ],
    [ "WindowErrorType", "_window_error_8h.html#abfcef8f0b709d9864e83e5b45b128ca3", [
      [ "WINDOW_CREATION_ERROR", "_window_error_8h.html#abfcef8f0b709d9864e83e5b45b128ca3a19f1c9591eae51ad50ae296a7c63ea4c", null ],
      [ "WINDOW_NOT_CREATED_ERROR", "_window_error_8h.html#abfcef8f0b709d9864e83e5b45b128ca3a48b791fd7c2d4920e6eab1ae73a5221f", null ],
      [ "WINDOW_SIZE_ERROR", "_window_error_8h.html#abfcef8f0b709d9864e83e5b45b128ca3a772f9c43ff0e30c738899507a55bb888", null ],
      [ "WINDOW_FUNCTION_NOT_AVAILABLE_ERROR", "_window_error_8h.html#abfcef8f0b709d9864e83e5b45b128ca3a9662e52d4a382a4a0457f0ae798a25a7", null ],
      [ "CONTEXT_NOT_SPECIFIED_ERROR", "_window_error_8h.html#abfcef8f0b709d9864e83e5b45b128ca3a07a225e408d5ad641f0790272758441e", null ],
      [ "INVALID_CONTEXT_ERROR", "_window_error_8h.html#abfcef8f0b709d9864e83e5b45b128ca3abfb238cbb075cc54cfb7a2e479f261ff", null ]
    ] ]
];