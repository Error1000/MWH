var struct_m_w_h_1_1_monitor =
[
    [ "Monitor", "struct_m_w_h_1_1_monitor.html#aaf9f5405828467dfe4fc99ec278f6828", null ],
    [ "getGLFWmonitor", "struct_m_w_h_1_1_monitor.html#a10e705823d39f5254c9b95103ee776d8", null ],
    [ "getHeightInMM", "struct_m_w_h_1_1_monitor.html#a32c44ee67662ce08461f49287d461747", null ],
    [ "getName", "struct_m_w_h_1_1_monitor.html#ae034b9e1ba51c8d68e992aa8b8b87161", null ],
    [ "getRefreshRateInHz", "struct_m_w_h_1_1_monitor.html#ab8c594d987d9669c303307499140a4fc", null ],
    [ "getResolutionHeightInPixels", "struct_m_w_h_1_1_monitor.html#aca2d1bf91e2ba35c011ced4fd4bfc3e4", null ],
    [ "getResolutionWidthInPixels", "struct_m_w_h_1_1_monitor.html#a0c89938620f336984155a3ba438a8487", null ],
    [ "getWidthInMM", "struct_m_w_h_1_1_monitor.html#abb5247201d75d3c25ef3d1e6326f1b22", null ],
    [ "getXPos", "struct_m_w_h_1_1_monitor.html#aae643eed3e3b71422ceb9d76c412bd78", null ],
    [ "getYPos", "struct_m_w_h_1_1_monitor.html#aa77778bae06b85d275212cf7b13dc45e", null ]
];