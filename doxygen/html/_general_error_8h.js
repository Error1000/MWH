var _general_error_8h =
[
    [ "GeneralError", "struct_m_w_h_1_1_general_error.html", "struct_m_w_h_1_1_general_error" ],
    [ "GeneralErrorType", "_general_error_8h.html#a4dad6f4a341016a9ca35ff1e93cd502f", [
      [ "UNKNOWN_ERROR", "_general_error_8h.html#a4dad6f4a341016a9ca35ff1e93cd502fac2d30ea2d94b3eb11abc82f215cd0ed3", null ],
      [ "PLATFORM_ERROR", "_general_error_8h.html#a4dad6f4a341016a9ca35ff1e93cd502fa7224e885f69fa0c0de6dc6a9ef6644d3", null ],
      [ "MWH_NOT_INITIALIZED_ERROR", "_general_error_8h.html#a4dad6f4a341016a9ca35ff1e93cd502fa89122c243a2bc4a455cd4890d5fff31a", null ],
      [ "NO_CONTEXT_ERROR", "_general_error_8h.html#a4dad6f4a341016a9ca35ff1e93cd502fa14625682e30239243b269f0f8ae418dc", null ]
    ] ]
];