var files_dup =
[
    [ "API.h", "_a_p_i_8h.html", "_a_p_i_8h" ],
    [ "APIError.h", "_a_p_i_error_8h.html", "_a_p_i_error_8h" ],
    [ "Monitor.h", "_monitor_8h.html", [
      [ "Monitor", "struct_m_w_h_1_1_monitor.html", "struct_m_w_h_1_1_monitor" ]
    ] ],
    [ "MonitorError.h", "_monitor_error_8h.html", "_monitor_error_8h" ],
    [ "Monitors.h", "_monitors_8h.html", "_monitors_8h" ],
    [ "MWHError.h", "_m_w_h_error_8h.html", [
      [ "MWHError", "struct_m_w_h_1_1_m_w_h_error.html", "struct_m_w_h_1_1_m_w_h_error" ]
    ] ],
    [ "Window.h", "_window_8h.html", [
      [ "Window", "class_m_w_h_1_1_window.html", "class_m_w_h_1_1_window" ]
    ] ],
    [ "WindowError.h", "_window_error_8h.html", "_window_error_8h" ]
];