var hierarchy =
[
    [ "exception", null, [
      [ "MWH::MWHError", "struct_m_w_h_1_1_m_w_h_error.html", [
        [ "MWH::APIError", "struct_m_w_h_1_1_a_p_i_error.html", null ],
        [ "MWH::MonitorError", "struct_m_w_h_1_1_monitor_error.html", null ],
        [ "MWH::WindowError", "struct_m_w_h_1_1_window_error.html", null ]
      ] ]
    ] ],
    [ "MWH::Monitor", "struct_m_w_h_1_1_monitor.html", null ],
    [ "MWH::API::Monitors", "struct_m_w_h_1_1_a_p_i_1_1_monitors.html", null ],
    [ "MWH::Window", "class_m_w_h_1_1_window.html", null ]
];