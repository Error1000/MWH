var _monitor_error_8h =
[
    [ "MonitorError", "struct_m_w_h_1_1_monitor_error.html", "struct_m_w_h_1_1_monitor_error" ],
    [ "MonitorErrorType", "_monitor_error_8h.html#a84fc01a9133b89614b8ea06b8012a686", [
      [ "MONITOR_INITIALIZATION_ERROR", "_monitor_error_8h.html#a84fc01a9133b89614b8ea06b8012a686a947aa854f5d16e400daa60b73adf2b6a", null ],
      [ "MONITOR_NOT_INITIALIZED_ERROR", "_monitor_error_8h.html#a84fc01a9133b89614b8ea06b8012a686a9501ddc68857100859b3b0e40d8a648b", null ]
    ] ]
];