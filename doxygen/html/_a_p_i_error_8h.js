var _a_p_i_error_8h =
[
    [ "APIError", "struct_m_w_h_1_1_a_p_i_error.html", "struct_m_w_h_1_1_a_p_i_error" ],
    [ "APIErrorType", "_a_p_i_error_8h.html#ae0de543ba4a94b8c655cce1ed3daff94", [
      [ "UNKNOWN_ERROR", "_a_p_i_error_8h.html#ae0de543ba4a94b8c655cce1ed3daff94ac2d30ea2d94b3eb11abc82f215cd0ed3", null ],
      [ "PLATFORM_ERROR", "_a_p_i_error_8h.html#ae0de543ba4a94b8c655cce1ed3daff94a7224e885f69fa0c0de6dc6a9ef6644d3", null ],
      [ "MWH_NOT_INITIALISED_ERROR", "_a_p_i_error_8h.html#ae0de543ba4a94b8c655cce1ed3daff94acfd2a5fe421d7658f3aeb456b125ad69", null ],
      [ "MWH_INITIALISED_ERROR", "_a_p_i_error_8h.html#ae0de543ba4a94b8c655cce1ed3daff94ab354974792cb8a6c880c61d731036d11", null ]
    ] ]
];