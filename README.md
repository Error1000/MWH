# MWH ( multi-platform window handler )

Progress until v1.0 stable: 75% ( currently on stall for switching to a new build system and a better library for the opengl test ( currently using glew, switching to glad ), this should not take long but progress is stalled until this switch is finished )

A simple multi-platform handles/wrapper for glfw that aims to make the use of glfw as transparent as possible so that even if mwh changes base libraries completly no changes to the api should need to be made. 

# Simple example of creating a no-context window

	#include <MWH/Window/Window.h>
		
	using namespace MWH;
	
	int main(){
	  API::init();
	  
	  Window win; // Create window object
	  win.createWindow(); // Actually create the window
	  
	  while(!win.shouldClose()){
		win.pollEvents(); // Update callbakcs and input and other things
		if(win.isKeyPressed(GLFW_KEY_ESCAPE)) win.setToClose();
	        win.update(); // Will only update frame for OPENGL context currently other contexts have to do updates manually but it will stil lcalculate fps
	  }
	  
	  win.destroyWindow();
	  API::exit();
	  // The window object goes out of scope here and is then deleted
	}

# Build library


 # GNU/Linux:
 Note: This command will download and install the required applications and libraries needed to build.
 Note: The automatron commands must be run from the project folder where this README is located!!!

   - Arch: ./automatron9000/automatron9000.sh -m release -o arch -l "$(cat Libraries/linux/arch/ilibs)"

   - Ubuntu: ./automatron9000/automatron9000.sh -m release -o ubuntu -l "$(cat Libraries/linux/ubuntu/ilibs)"

 # Windows ( using make to build and the chocolatey package manager to install stuff, using the libraries from the Libraries\windows folder provided ):
  Note: This command will download and install the required applications needed to build this project and it will also install chocolatey.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - Open a cmd as administrator and run: for /F "usebackq delims=" %A in (\`type Libraries\windows\ilibs\`) do automatron9000\automatron9000.bat -m release -o windows -l %A

 # MacOS ( using make to build and brew package manager to get libraries and install stuff ):
  Note: This command will download and install the required applications needed to build and install brew.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - ./automatron9000/automatron9000.sh -m release -o macos -l "$(cat Libraries/macos/ilibs)"



# Build debug(opengl)


 # GNU/Linux:
 Note: This command will download and install the required applications and libraries needed to build.
 Note: The automatron commands must be run from the project folder where this README is located!!!

   - Arch: ./automatron9000/automatron9000.sh -m debugopengl -o arch -l "$(cat Libraries/linux/arch/ilibs)"

   - Ubuntu: ./automatron9000/automatron9000.sh -m debugopengl -o ubuntu -l "$(cat Libraries/linux/ubuntu/ilibs)"

 # Windows ( using make to build and the chocolatey package manager to install stuff, using the libraries from the Libraries\windows folder provided ):
  Note: This command will download and install the required applications needed to build this project and it will also install chocolatey.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - Open a cmd as administrator and run: for /F "usebackq delims=" %A in (\`type Libraries\windows\ilibs\`) do automatron9000\automatron9000.bat -m debugopengl -o windows -l %A

 # MacOS ( using make to build and brew package manager to get libraries and install stuff ):
  Note: This command will download and install the required applications needed to build and install brew.
  Note: The automatron commands must be run from the project folder where this README is located!!!

   - ./automatron9000/automatron9000.sh -m debugopengl -o macos -l "$(cat Libraries/macos/ilibs)"


# Build debug(vulkan)

 # GNU/Linux:
 Note: This command will download and install the required applications and libraries needed to build.
 Note: The automatron commands must be run from the project folder where this README is located!!!

   - Arch: ./automatron9000/automatron9000.sh -m debugvulkan -o arch -l "$(cat Libraries/linux/arch/ilibs)"

   - Ubuntu: ./automatron9000/automatron9000.sh -m debugvulkan -o ubuntu -l "$(cat Libraries/linux/ubuntu/ilibs)"
