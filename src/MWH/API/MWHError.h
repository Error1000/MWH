#pragma once

#include <stdexcept>
#include <iostream>
#include <string>

/**
    @file MWHError.h

    @brief Contains all the definitions and declarations of a base "error" tamplate from which all other errors inherit
*/



namespace MWH{

/**
    @struct MWH::MWHError

    @brief Base of all api errors
*/
struct MWHError : std::exception{
const std::string msg;

explicit MWHError(const std::string& message):
    msg(message) {}

private:
    /**
        @brief Prints the error message that was passed to the error when it was initialized if it hasn't been catched
    */
    const char* what() const noexcept{ return msg.c_str(); }
};

}
