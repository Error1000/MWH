#pragma once

#include <GLFW/glfw3.h>
#include "APIError.h"

/**
 @file API.h

 @brief Contains definitions of public variables/functions for the API and includes most necessary files needed by basically all files in this project
*/



namespace MWH{

// Forward declarations ( to make sure the monitor class can befriend the Widnow cllass so that they can share private variables without exposing them to the user )
// We just do all classes ( that are not errors ) here because it's neater and avoids confusion
class Window;
struct Monitor;
class Cursor;
struct Image;

namespace API{

/**
    @brief This function initializes the API and it's components, many of the functions that the API uses can not be used before this function is called!

    @throw MWH::APIError Should only be thrown if the platform is unsupported.


    @warning On OS X, this function will change the current directory of the application to the Contents/Resources subdirectory of the application's bundle, if present. This can be disabled with a compile-time option when compiling glfw.
*/
void init();


/**
    @brief Wheter or not the api is initialised

    @retval bool Wheter or not the api is initialised
*/
bool isInitialised();


/**
    @brief Uninitializes the API, after this most API functions can not be used anymore

    @throw MWH::APIError Should only be thrown if the platform is unsupported or the API hasn't been initialized yet.


    @warning This function can not be called from a callback

    @remarks There is no need to call this function if the API fails to initialize
*/
void exit();


/**
    @brief Returns the time passed since the API was initialised

    @note This function is ment to be used for time sensitive actions related to graphics or the window, like calcualting fps, or making smooth animations.

    @throw MWH::APIError Should only be thrown if the API hasn't been initialized yet

    @warning This might not necessearly return the time passed since the API was initialised, it might also return something else since that value can be changed using internal functions.
*/
inline const double getCurrentTime(){ return glfwGetTime(); }


}

}
