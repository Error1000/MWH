#pragma once

#include "MWHError.h"

/**
    @file APIError.h

    @brief Contains all the definitions and declarations of API Error related stuff
*/



namespace MWH{

/**
    @enum MWH::APIErrorType

    @brief A list of all API Errors that can occur
*/
enum APIErrorType{
    UNKNOWN_ERROR, PLATFORM_ERROR, MWH_NOT_INITIALISED_ERROR, MWH_INITIALISED_ERROR
};

/**
    @struct MWH::APIError

    @brief Thrown whenever a general error occurs like a platform error, an uninitialized error, an unknown error, etc.
*/
struct APIError : MWH::MWHError{
const MWH::APIErrorType errorType;

explicit APIError(const MWH::APIErrorType& et, const std::string& message):
   MWH::MWHError(message), errorType(et) {}
};

}
