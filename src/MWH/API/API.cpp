#include "API.h"

// NOTE: This is static to make sure that these can not be seen by anybody else
static bool apiInitialised = false;

static void errorCallback(int error, const char* description){
    switch(error){
        case GLFW_PLATFORM_ERROR:
            throw MWH::APIError(MWH::PLATFORM_ERROR, description);
        break;

        case GLFW_NOT_INITIALIZED:
            throw MWH::APIError(MWH::MWH_NOT_INITIALISED_ERROR, description);
        break;

        case GLFW_INVALID_ENUM:
            throw std::invalid_argument(description);
        break;

        default:
            throw MWH::APIError(MWH::UNKNOWN_ERROR, description);
        break;
    }

}

void MWH::API::init(){
    if(apiInitialised) throw MWH::APIError(MWH::MWH_INITIALISED_ERROR, "The api is already initialised!");
    //NOTE: The glfwSetErrorCallaback function can be set before initializing the library, this is mentioned in the docs
    glfwSetErrorCallback(errorCallback);
    if(!glfwInit()) throw MWH::APIError(MWH::UNKNOWN_ERROR, "An unknown error occured while initalizing glfw!");

    apiInitialised = true;
}

bool MWH::API::isInitialised(){ return apiInitialised; }

void MWH::API::exit(){
    if(!apiInitialised) throw MWH::APIError(MWH::MWH_NOT_INITIALISED_ERROR, "Can't uninitialize the API before it's initialized!");
    ///NOTE: The contexts of any remaining windows must not be current on any other thread when this function is called.
    glfwTerminate();
    apiInitialised = false;
}
