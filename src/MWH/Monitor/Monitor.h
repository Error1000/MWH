#pragma once

#include "../API/API.h"
#include "MonitorError.h"


/**
    @file Monitor.h

    @brief Contains all definitions/declarations of Monitor realted things

*/



namespace MWH{


/**

    @struct MWH::Monitor

    @brief An object that contains all the properties/variables of a monitor and what you can do with it

*/
struct Monitor{
private:
    GLFWmonitor* mon;

public:
    /**
        @throw MWH::MonitorError Should only be thrown when trying to initiate monitor with a nullpointer

        @brief A constructor to initalize a monitor object from a glfw monitor object
    */
    Monitor(GLFWmonitor* m):
    mon(m){
        if(mon == nullptr) throw MWH::MonitorError(MWH::MONITOR_INITIALIZATION_ERROR, "Can not initialize monitor as null!");
    }

    /// Note: No need for destructor since glfw automatically deletes the monitor pointer when it goes out of scope


    /**
        @brief Returns the name that the os gives the monitor

        @warning This will not necessarily return the monitor model

        @retval std::string The name of the monitor
    */
    inline const std::string getName(){
        return glfwGetMonitorName(Monitor::mon);
    }

    /**
        @retval int The height of the current reolution of the monitor in Screen Coordinates ( SC ) ( usually pixels )
    */
    inline const int getResolutionHeightInSC(){
        return glfwGetVideoMode(Monitor::mon)->height;
    }

    /**
        @retval int The width of the current reolution of the monitor in Screen Coordinates ( SC ) ( usually pixels )
    */
    inline const int getResolutionWidthInSC(){
        return glfwGetVideoMode(Monitor::mon)->width;
    }

    /**
        @retval int The height of the monitor in millimeters
    */
    inline const int getHeightInMM(){
        int heightMM;
        glfwGetMonitorPhysicalSize(Monitor::mon, nullptr, &heightMM);
        return heightMM;
    }

    /**
        @retval int The width of the monitor in millimeters
    */
    inline const int getWidthInMM(){
        int widthMM;
        glfwGetMonitorPhysicalSize(Monitor::mon, &widthMM, nullptr);
        return widthMM;
    }

    /**
        @retval int The refresh rate of the monitor in hz
    */
    inline const int getRefreshRateInHz(){
        return glfwGetVideoMode(Monitor::mon)->refreshRate;
    }

    /**
        @brief  Returns the distance in screen coordinates from the top left corner of the middle monitor according to the os

        @warning This will not necessarily return the physical location of the monitor

        @retval int The x pos
    */
    inline const int getXPos(){
        int xPos;
        glfwGetMonitorPos(Monitor::mon, &xPos, nullptr);
        return xPos;
    }

    /**
        @brief Returns the distance in screen coordinates from the top left corner of the middle monitor according to the os

        @warning This will not necessarily return the physical location of the monitor

        @retval int The y pos
    */
    inline const int getYPos(){
        int yPos;
        glfwGetMonitorPos(Monitor::mon, nullptr, &yPos);
        return yPos;
    }


    /**
        @brief Returns the depth ( size ) of the blue channel of the monitor, eg. 8 = 8 bytes for that channel, or a max of 256 colors for that channel

        @return int The depth of the blue channel in bytes
    */
    inline const int getBuleBitDepth(){ return glfwGetVideoMode(Monitor::mon)->blueBits; }


    /**
        @brief Returns the depth ( size ) of the red channel of the monitor, eg. 8 = 8 bytes for that channel, or a max of 256 colors for that channel

        @return int The depth of the red channel in bytes
    */
    inline const int getRedBitDepth(){ return glfwGetVideoMode(Monitor::mon)->redBits; }


    /**
        @brief Returns the depth ( size ) of the green channel of the monitor, eg. 8 = 8 bytes for that channel, or a max of 256 colors for that channel

        @return int The depth of the green channel in bytes
    */
    inline const int getGreenBitDepth(){ return glfwGetVideoMode(Monitor::mon)->greenBits; }


    friend MWH::Window; // Let the MWH::Window class access private variables

};

}
