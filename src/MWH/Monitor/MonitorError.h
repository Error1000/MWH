#pragma once

#include "../API/MWHError.h"

/**
    @file MonitorError.h

    @brief Contains all the definitions and declarations of Monitor Error related stuff
*/


namespace MWH{

/**
    @enum MWH::MonitorErrorType

    @brief A list of all Monitor Errors that can occur
*/
enum MonitorErrorType{
    MONITOR_INITIALIZATION_ERROR, MONITOR_NOT_INITIALIZED_ERROR
};


/**
    @struct MWH::MonitorError

    @brief Thrown whenever a monitor specific error occurs
*/
struct MonitorError : MWH::MWHError{
const MWH::MonitorErrorType errorType;


explicit MonitorError(const MWH::MonitorErrorType& et, const std::string& message):
   MWH::MWHError(message), errorType(et) {}
};

}
