#pragma once
#include "../API/API.h"
#include "Monitor.h"


/**
    @file Monitors.h

    @brief Contains definitions of public Monitor related functions and definitions and declarations of public Monitor related variables
*/

// Note: These public functions have to be declared somewhere else as they are not contained in anything but a namespace

namespace MWH{


namespace API{


/**

    @struct MWH::API::Monitors

    @brief A struct containing a pointer to an array of MWH::Monitor and an integer which represents the length of that array

*/
struct Monitors{
 /**
    @brief A pointer to an array of n monitors
 */
 MWH::Monitor** monitors;
 /**
    @brief The number of monitors in the array
 */
 int n;

 /**
    @brief Delete all monitor pointers to MWH::Monitor objects when the API::Monitors object goes out of copse
 */
 ~Monitors(){
    // Delete all refrences to the monitors that we created to make sure that glfw can delete the monitors if possible
    for(int i = 0; i < Monitors::n; ++i) delete Monitors::monitors[i];
    delete Monitors::monitors;
 }

};


/**
    @brief Gets a list of all the monitors connected to the computer

    @throw MWH::APIError Can throw an exception if the api is not initialized

    @warning DO NOT free the values in the struct yourself the api does that for you

    @retval MWH::Monitors A struct containing a pointer to an array of MWH::Monitor and an integer which represents the length of that array
*/
inline API::Monitors getMonitors(){
    int n;
    // Note: This array is automatically freed by glfw when the glfw monitors have no refrences left to them
    GLFWmonitor** monitors = glfwGetMonitors(&n);

    // Copy glfw monitor array to a MWH::Monitor array
    Monitor** m = new Monitor*[n];
    for(int i = 0; i < n; ++i) m[i] = new Monitor(monitors[i]);

    return MWH::API::Monitors{m /* monitor array */,
                     n /* nr. of monitors */ };

}


/**
    @brief Returns what the os/api thinks is the main monitor of the computer

    @throw MWH::APIError Can throw an exception if the api is not initialized

    @remarks The monitor returned is always the first monitor in the array that the MWH::API::getMonitors() functions would have returned

    @retval MWH::Monitor* A pointer to a monitor
*/
inline MWH::Monitor getPrimaryMonitor(){ return MWH::Monitor(glfwGetPrimaryMonitor()); }

}

}
