#include <GL/glew.h>
#include "API/API.h"
#include "Window/Window.h"
#include "Monitor/Monitors.h"
#include <iostream>
#include <string>
#include <vector>
#include <random>


/// Note: These are static just to make sure they can only be used by this file
static const bool initGLEW();
static const GLuint loadVertexAndFragmentShaders(const std::string vertex_shader, const std::string fragment_shader);



int main(){
MWH::API::init();
MWH::Window win(MWH::Window::OPENGL_CONTEXT), win2(MWH::Window::OPENGL_CONTEXT);

win.setResizeable(true);
//Resize function ( lambda function )
win.setResizeCallback([](MWH::Window* w){glViewport(0, 0, w->getWidth(), w->getHeight()); /* assumes screen coordinates are pixels */ });
win.setFileOrFolderDroppedCallback([](MWH::Window* w, int c, std::string* paths){ for(int i = 0; i < c; ++i) std::cout << paths[i] << '\n'; std::cout << w->getMouseXPos() << ' ' << w->getMouseYPos() << '\n'; });
win.setVSync(false);
win.setTitle("Window 1");
win.setPos(win.getMonitor()->getResolutionWidthInSC()/2 - win.getWidth(), win.getMonitor()->getResolutionHeightInSC()/2 - win.getHeight()/2);
win.createWindow(); /// Note: This will also switch context focus to the newly created window

// Initialize glew to have better access to what functions are available
// Note: At least one window needs to exists to initialise this, and it's not context dependant ( it just defines opengl functions, the context is handeld by MWH )
const bool result = initGLEW();
if(!result){
        MWH::API::exit();
        return -1;
}

// Triangle shape
// Note: We don't use indicies for this since it's overkill

// Create array of buffers that hold the vertx attributes in the currently bound context
GLuint vertArrayID;
glGenVertexArrays(1 /* how many arrays */, &vertArrayID /* pointer to first element of array of the vertexarrays */ );
// Tell opengl to use this vao in any other subsequent calls in the currently bound context
glBindVertexArray(vertArrayID);


static const GLfloat vertex_positions_data[] = {
    -1.0f, -1.0f, // left bottom
    0.0f, 1.0f, // top
    1.0f, -1.0f, // right bottom
};

// Create a buffer to contain the vertex positions for this and all contexts that share with this one
GLuint vertexPositionBufferID;
glGenBuffers(1 /* how many buffers */, &vertexPositionBufferID);
// Tell opengl to use this vbo in any other subsequent calls in the currently bound context
glBindBuffer(GL_ARRAY_BUFFER /* buffer type */, vertexPositionBufferID);
// Upload vertices to the gpu
glBufferData(GL_ARRAY_BUFFER /* buffer type */, sizeof(vertex_positions_data) /* how many bytes to upload */, vertex_positions_data /* the data as a pointer */, GL_STATIC_DRAW /* what we are going to do with the data */);


// Enable the use of the element at index 0 in the currently bound vao in the currently bound context
glEnableVertexAttribArray(0);

// Add pointer to vertex positions of bound vbo in the vao which contains the vertx attributes in the currently bound context
glVertexAttribPointer(
                      0 /* index in array */,
                      2 /* number of data points per vertex, since we are uploading positions this will be 2 ( 2 data points = 2 numbers = 2d coordinate ) */,
                      GL_FLOAT /* data type */,
                      GL_FALSE /* normalised? */,
                      0 /* stride */,
                      (void*) 0 /* offset */
                      );

// Basic Shaders
// Hard coded because it's easier
const std::string vs = std::string("#version 330 core\n") +
                       std::string("layout(location = 0) in vec2 vertPos;\n")+
                       std::string("\n")+
                       std::string("void main(){\n")+
                       std::string("gl_Position = vec4(vertPos, 1, 1);\n")+
                       std::string("}\n");

const std::string fs = std::string("#version 330 core\n") +
                       std::string("out vec3 color;\n")+
                       std::string("\n")+
                       std::string("void main(){\n")+
                       std::string("color = vec3(1, 0, 0);\n")+
                       std::string("}\n");

GLuint programID = loadVertexAndFragmentShaders(vs, fs); // Compile link and create the program from the shaders for this and all contexts that share with this one

// Bind shaders for this context
glUseProgram(programID);

// Set clear color for this context
glClearColor(255, 255, 255, 255 /* white */ );


win2.setResizeable(true);
win2.setResizeCallback([](MWH::Window* w){glViewport(0, 0, w->getWidth(), w->getHeight());});
win2.setFileOrFolderDroppedCallback([](MWH::Window* w, int c, std::string* paths){ for(int i = 0; i < c; ++i) std::cout << paths[i] << '\n'; std::cout << w->getMouseXPos() << ' ' << w->getMouseYPos() << '\n'; });
win2.setVSync(false);
win2.setTitle("Window 2");
win2.setPos(win2.getMonitor()->getResolutionWidthInSC()/2, win2.getMonitor()->getResolutionHeightInSC()/2 - win2.getHeight()/2);
win2.shareContextWith(win); // Share contexts
win2.createWindow(); /// Note: This will also switch context focus to the newly created window

//In the win2 opengl context that dosen't have anything bound but has all the data that is shared from win's context
//------------------------------------------------------------------------------------------------------------------

// Create array of buffers that hold the vertices for the other context
GLuint vertArrayID2;
glGenVertexArrays(1 /* how many arrays */, &vertArrayID2 /* pointer to first element of array of the vertexarrays */ );

// Tell opengl to use this vao in any other subsequent calls that might need one in the currently bound context
glBindVertexArray(vertArrayID2);

// Tell opengl to use this vbo ( the one shared from the other context ) in any other subsequent calls in the currently bound context
glBindBuffer(GL_ARRAY_BUFFER /* buffer type */, vertexPositionBufferID);

// Enable the use of the element at index 0 in the currently bound vao in the current context
glEnableVertexAttribArray(0);

// Add pointer of bound ( shared from other context ) vbo in the vao
glVertexAttribPointer(
                      0 /* index in array */,
                      2 /* number of data points per vertex ( 2 data points = 2 numbers = 2d coordinate */,
                      GL_FLOAT /* data type */,
                      GL_FALSE /* normalised? */,
                      0 /* stride */,
                      (void*) 0 /* offset */
                     );

// Bind shaders ( shared fro mother context )
glUseProgram(programID);

// Set clear color for this context
glClearColor(0, 0, 0, 255 /* black */ );

//------------------------------------------------------------------------------------------------------------------

bool alive1 = true;
bool alive2 = true;

while(alive1 || alive2){
    if(alive1 && !win.shouldClose()){
        win.setContextFocus();
        win.pollEvents();

        //Clear last frame
        glClear(GL_COLOR_BUFFER_BIT);

        //Draw triangle
        glDrawArrays(GL_TRIANGLES, 0, 3);

        if(win.isKeyPressed(GLFW_KEY_ESCAPE)) win.setToClose();

        std::cout << "Win1: "<< win.getFPS() << " fps" << '\n';
        win.update();
    }else if(alive1){
        win.setContextFocus();
        glDisableVertexAttribArray(0); // Siable the use of index 0 of the currently bound vao
        win.destroyWindow();
        alive1 = false;
    }

    if(alive2 && !win2.shouldClose()){
        win2.setContextFocus();
        win2.pollEvents();
        //Clear last frame
        glClear(GL_COLOR_BUFFER_BIT);

        //Draw triangle
        glDrawArrays(GL_TRIANGLES, 0, 3);

        if(win2.isKeyPressed(GLFW_KEY_ESCAPE)) win2.setToClose();

        std::cout << "Win2: " << win2.getFPS() << " fps" << '\n';
        win2.update();
    }else if(alive2){
        win2.setContextFocus();
        glDisableVertexAttribArray(0);
        win2.destroyWindow();
        alive2 = false;
    }

}


MWH::API::exit();

}



static const bool initGLEW(){

glewExperimental = GL_TRUE;
GLenum state = glewInit();
if(state == GLEW_ERROR_NO_GL_VERSION){
   std::cerr << "Your graphics card isn't supported / dosen't work with opengl ( no available opengl version found ) !" << '\n';
   std::cerr << "GLEW Version: " + std::string((char*)glewGetString(GLEW_VERSION)) << '\n';

   return false;
}else if(state != GLEW_OK){
   std::cerr << "GLEW Version: " + std::string((char*)glewGetString(GLEW_VERSION)) << '\n';

   return false;
}

return true;
}


static const GLuint loadVertexAndFragmentShaders(const std::string vertex_shader, const std::string fragment_shader){

	// Create the shaders
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	GLint shaderCompiled;
	int infoLogLength;

	// Compile Vertex Shader
	const char* vertexSourcePointer = vertex_shader.c_str();
	glShaderSource(vertexShaderID, 1, &vertexSourcePointer, NULL);
	glCompileShader(vertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &shaderCompiled);
	glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if(shaderCompiled != GL_TRUE){
		std::vector<char> VertexShaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(vertexShaderID, infoLogLength, NULL, &VertexShaderErrorMessage[0]);
		std::cerr << &VertexShaderErrorMessage[0] << '\n';
		throw std::runtime_error("Vertex shader compilation failed!");
	}

	// Compile Fragment Shader
	const char* fragmentSourcePointer = fragment_shader.c_str();
	glShaderSource(fragmentShaderID, 1, &fragmentSourcePointer, NULL);
	glCompileShader(fragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &shaderCompiled);
	glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if( shaderCompiled != GL_TRUE ){
		std::vector<char> FragmentShaderErrorMessage(infoLogLength+1);
		glGetShaderInfoLog(fragmentShaderID, infoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		std::cerr << &FragmentShaderErrorMessage[0] << '\n';
		throw std::runtime_error("Fragment shader compilation failed!");
	}

	// Link and create the program
	GLuint programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
	glLinkProgram(programID);

	// Check the program
	glGetProgramiv(programID, GL_LINK_STATUS, &shaderCompiled);
	glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if(shaderCompiled != GL_TRUE){
		std::vector<char> ProgramErrorMessage(infoLogLength+1);
		glGetProgramInfoLog(programID, infoLogLength, NULL, &ProgramErrorMessage[0]);
		std::cerr << &ProgramErrorMessage[0] << '\n';
		throw std::runtime_error("Shader linking failed!");
	}

	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID);

	glDeleteShader(vertexShaderID);
	glDeleteShader(fragmentShaderID);

	return programID;
}

