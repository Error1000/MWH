#pragma once

#include "../API/MWHError.h"

/**
    @file WindowError.h

    @brief Contains all the definitions and declarations of Window Error related stuff
*/



namespace MWH{

/**
    @enum MWH::WindowErrorType

    @brief A list of all Window Errors that can occur
*/
enum WindowErrorType{
    WINDOW_CREATION_ERROR, WINDOW_NOT_CREATED_ERROR, WINDOW_SIZE_ERROR, WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, CONTEXT_NOT_SPECIFIED_ERROR, INVALID_CONTEXT_ERROR
};


/**
    @struct MWH::WindowError

    @brief Thrown whenever a window specific error occurs
*/
struct WindowError : MWH::MWHError{
const MWH::WindowErrorType errorType;

explicit WindowError(const MWH::WindowErrorType& et, const std::string& message):
   MWH::MWHError(message), errorType(et) {}
};

}

