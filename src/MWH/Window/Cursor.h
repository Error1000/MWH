#pragma once
#include "../API/API.h"
#include "Image.h"

namespace MWH{

class Cursor{

private:
    GLFWcursor* cursor = nullptr;

public:

    enum SystemShape : int{
        ARROW_CURSOR = GLFW_ARROW_CURSOR, IBEAM_CURSOR = GLFW_IBEAM_CURSOR, CROSSHAIR_CURSOR = GLFW_CROSSHAIR_CURSOR, HAND_CURSOR = GLFW_HAND_CURSOR, HEIGHT_RESIZE_CURSOR = GLFW_HRESIZE_CURSOR, VERTICAL_RESIZE_CURSOR = GLFW_VRESIZE_CURSOR,
    };

    enum Mode : int{
        CURSOR_NORMAL = GLFW_CURSOR_NORMAL, CURSOR_HIDDEN = GLFW_CURSOR_HIDDEN /* Hides the cursor but dosen't stop it from going outside the window */, CURSOR_DISABLED = GLFW_CURSOR_DISABLED /* Hides and steals(grabs) the cursor so it can't exit the window */
    };

    Cursor(const MWH::Image& img, const int xHotSpot = 0, const int yHotSpot = 0):
    cursor(glfwCreateCursor(&img, xHotSpot, yHotSpot)){}

    Cursor(const SystemShape& shp):
    cursor(glfwCreateStandardCursor(shp)){}

    ~Cursor(){ if(MWH::API::isInitialised()) glfwDestroyCursor(cursor); }

    friend MWH::Window; // Let the MWH::Window access the underlying glfw pointer
};

}

