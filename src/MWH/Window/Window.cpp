#include "Window.h"
#include <sstream>
#include <string>
#include <iostream>
#include <stdexcept>

using namespace MWH;


// Definitions of functions

void Window::createWindow(){
        if(Window::window != nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "Window already exists, co can't create window!");

      	// Just to make sure that the what MWH thinks is the window size is the actual window size
		// This is done just because it is used in calculations and it's a good idea for the two things to be the same
		// It's also here in case someone sets the width and height of the window and also makes it full-screen at the same time
		if(Window::fullscreen){
			Window::width =  Window::monitor->getResolutionWidthInSC();
			Window::height = Window::monitor->getResolutionHeightInSC();
		}

        // Set monitor settings
		glfwWindowHint(GLFW_RED_BITS, Window::monitor->getRedBitDepth());
		glfwWindowHint(GLFW_GREEN_BITS, Window::monitor->getGreenBitDepth());
		glfwWindowHint(GLFW_BLUE_BITS, Window::monitor->getBuleBitDepth());
		glfwWindowHint(GLFW_REFRESH_RATE, Window::monitor->getRefreshRateInHz());

        // Make sure the window is invisible when we make it so that we can change size and other things without it "spazing" out on the screen while we do so
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

		glfwWindowHint(GLFW_MAXIMIZED, Window::maximised ? GLFW_TRUE : GLFW_FALSE);

		glfwWindowHint(GLFW_RESIZABLE, Window::resizeable ? GLFW_TRUE : GLFW_FALSE);


		Window::window = glfwCreateWindow(Window::width, Window::height, Window::name.c_str(), (Window::fullscreen ? Window::monitor->mon : nullptr), ((Window::toShareWith == nullptr) ? nullptr : Window::toShareWith->window));

        if(window == nullptr) throw WindowError(WINDOW_CREATION_ERROR, "MWH failed to create window!");

		if(Window::context == Window::OPENGL_CONTEXT) glfwMakeContextCurrent(Window::window); // Set context focus

        glfwSetWindowUserPointer(Window::window, this); // Assign this object to the window object so given any glfw window object we can get the MWH window object if it was made using an MWH window object ( also useful when usign callbacks since they only give you the glfw window and no way to pass other data other than the user pointer )


        // Set callbacks
        // Note: We use glfwSetWindowSizeCallback instead of the frame buffer one because the most other things are measured in screen coordinates, and setting the window size takes screeen coordinates and not pixels
        glfwSetWindowSizeCallback(Window::window, [](GLFWwindow* win, int newWidth, int newHeight){
                MWH::Window* w = static_cast<MWH::Window*>(glfwGetWindowUserPointer(win));
                w->setContextFocus();
                w->width = newWidth;
                w->height = newHeight;
                if(w->onWindowResizeFunc != nullptr) w->onWindowResizeFunc(w);
        });

        glfwSetDropCallback(Window::window, [](GLFWwindow* win, int c, const char** paths){
                MWH::Window* w = static_cast<MWH::Window*>(glfwGetWindowUserPointer(win));
                std::string* strPaths = new std::string[c];
                for(int i = 0; i < c; i++) strPaths[i] = std::string(paths[i]);
                if(w->onFileOrFolderDroppedOnWindow != nullptr) w->onFileOrFolderDroppedOnWindow(w, c, strPaths);
        });

        glfwSetWindowPosCallback(Window::window, [](GLFWwindow* win, int newXPos, int newYPos){
                MWH::Window* w = static_cast<MWH::Window*>(glfwGetWindowUserPointer(win));
                w->xPos = newXPos - (w->monitor->getXPos());
                w->yPos = newYPos - (w->monitor->getYPos());
                if(w->onWindowPosChangeFunc != nullptr) w->onWindowPosChangeFunc(w);
        });

        glfwSetCursorPosCallback(Window::window, [](GLFWwindow* win, double newMouseXPos, double newMouseYPos){
                MWH::Window* w = static_cast<MWH::Window*>(glfwGetWindowUserPointer(win));
                w->mouseXPos = newMouseXPos;
                w->mouseYPos = newMouseYPos;
                if(w->onCursorPosChangeFunc != nullptr) w->onCursorPosChangeFunc(w);
        });

        glfwSetKeyCallback(Window::window, [](GLFWwindow* win, int key, int keyScanCode, int action, int modifiers){
                MWH::Window* w = static_cast<MWH::Window*>(glfwGetWindowUserPointer(win));
                if(w->onKeyboardKeyChangeFunc != nullptr) w->onKeyboardKeyChangeFunc(w, key, keyScanCode, action, modifiers);

        });

        glfwSetMouseButtonCallback(Window::window, [](GLFWwindow* win, int key, int action, int modifiers){
                MWH::Window* w = static_cast<MWH::Window*>(glfwGetWindowUserPointer(win));
                if(w->onMouseKeyChangeFunc != nullptr) w->onMouseKeyChangeFunc(w, key, action, modifiers);
        });


        glfwSetCursorEnterCallback(Window::window, [](GLFWwindow* win, int state){
                MWH::Window* w = static_cast<MWH::Window*>(glfwGetWindowUserPointer(win));
                const bool isInside = (state == GLFW_TRUE);
                w->cursorInside = isInside;
                if(w->onCursorEnterOrLeaveFunc != nullptr) w->onCursorEnterOrLeaveFunc(w);
        });


		glfwSetWindowPos(Window::window, Window::xPos + (Window::monitor->getXPos()), Window::yPos + (Window::monitor->getYPos()));

		if(Window::icons != nullptr) glfwSetWindowIcon(Window::window, Window::iconCount, Window::icons);

        if(Window::cursor != nullptr) glfwSetCursor(Window::window, Window::cursor->cursor);

        glfwSetInputMode(Window::window, GLFW_CURSOR, Window::cursorMode);

		// Finally we show the window
		if(!Window::hidden) glfwShowWindow(Window::window);

		if(Window::minimised) glfwIconifyWindow(Window::window);

		//Set vsync ( only works for graphics frameworks better supported by glfw )
		if(Window::context == Window::OPENGL_CONTEXT){
            if(vsync) glfwSwapInterval(1); else glfwSwapInterval(0);
		}

}

void Window::destroyWindow(){
    if(Window::window == nullptr) throw WindowError(WINDOW_NOT_CREATED_ERROR, "Can't destory a non-existant window!");
    glfwDefaultWindowHints();
    glfwDestroyWindow(Window::window);
    Window::mouseXPos = 0;
    Window::mouseYPos = 0;

    Window::lastTime = glfwGetTime();
    Window::lastSecond = glfwGetTime();
    Window::fps = 0;
    Window::nbFrames = 0;

    Window::deltaTime = 0;
    Window::totalDeltas = 0;
    Window::deltas = 0;

    Window::window = nullptr;
}

void Window::shareContextWith(MWH::Window& other){
    if(Window::window != nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "Can't set the window to share the context with after this one was created!");
    if(Window::context != other.context) throw WindowError(INVALID_CONTEXT_ERROR, "Can't share contexts between different context types!");

    Window::toShareWith = &other;
}

void Window::setContext(const Window::Context newContext){
    if(Window::window != nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "Can't set window context after window has been created!");
    Window::context = newContext;
    switch(Window::context){
            case Window::OPENGL_CONTEXT:
                glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
                glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
                glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
                glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
                glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            break;

            case Window::VULKAN_CONTEXT:
            case Window::NO_CONTEXT:
                glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
            break;

            default:
                glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
                throw std::invalid_argument("Please specify a context to use, using no context by default!");
            break;
    }
}


void Window::update(){
    if(Window::window == nullptr) throw WindowError(WINDOW_NOT_CREATED_ERROR, "Can't update window since it dosen't exist!");

	// Swap drawing buffers ( change current frame for the next painted frame ), only works for graphics libraries that have support for some functions in glfw
	if(Window::context == Window::OPENGL_CONTEXT) glfwSwapBuffers(Window::window);


    // Stuff to do with frame rate

    //Calcualte delta since last frames
    const double currentTime = API::getCurrentTime();
	Window::deltaTime = currentTime - Window::lastTime;
	Window::totalDeltas += Window::deltaTime;
	++Window::deltas;
	Window::lastTime = currentTime;


    //Calcualte fps

	//If more that 1 second passed since the last time we recorded a frame
	if (currentTime - Window::lastSecond >= 1.0) {
        //Change fps value
		Window::fps = Window::nbFrames;

		//Reset numeber of frames that have past sicne the last frame
		Window::nbFrames = 0;

		//Reset the number of seconds that have passed since the last frame
		Window::lastSecond = currentTime;
	}

	//Increase the number of frames that have passed by
	++Window::nbFrames;

}

void Window::setToClose(bool close){
    if(Window::window == nullptr) throw WindowError(WINDOW_NOT_CREATED_ERROR, "The window can't be set to close before the window has been created!");
    glfwSetWindowShouldClose(Window::window, close ? GLFW_TRUE : GLFW_FALSE);
}

bool Window::shouldClose(){
    if(Window::window == nullptr) throw WindowError(WINDOW_NOT_CREATED_ERROR, "Can't check wheter or not the window should close since it hasn't been created!");
    return glfwWindowShouldClose(Window::window);
}

void Window::setMonitor(MWH::Monitor* m){
    if(Window::window != nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "Can't set window monitor after the window has been created!");
    Window::monitor = m;
}

void Window::setResizeable(const bool isResizeable){
    if(Window::window != nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "The window can't be set to be resizeable after the window has been created has started!");
    Window::resizeable = isResizeable;

}

void Window::setHidden(const bool hide){
    if(Window::window != nullptr){
        if(!Window::hidden && hide)
            glfwHideWindow(Window::window);
        else if(Window::hidden && !hide)
            glfwShowWindow(Window::window);
    }
    Window::hidden = hide;
}

void Window::setMinimised(const bool minimise){
    if(Window::window != nullptr){
        if(minimise)
            glfwIconifyWindow(Window::window);
        else
            glfwRestoreWindow(Window::window);
    }
    Window::minimised = minimise;
}

void Window::setMaximised(const bool maximise){
    if(Window::window != nullptr){
        if(maximise)
            glfwMaximizeWindow(Window::window);
        else
            glfwRestoreWindow(Window::window);
    }
    Window::maximised = maximise;
}

void Window::setIcon(MWH::Image* newIcons, int c){
    if(c < 1) throw std::invalid_argument("Icon set size can't be 0, there must be at least 1 icon!");
    Window::iconCount = c;
    Window::icons = newIcons;
    if(Window::window != nullptr) glfwSetWindowIcon(Window::window, Window::iconCount, Window::icons);
}

void Window::setCursor(MWH::Cursor* newCursor){
    Window::cursor = newCursor;
    if(Window::window != nullptr) glfwSetCursor(Window::window, Window::cursor->cursor);
}

void Window::setCursorMode(MWH::Cursor::Mode mode){
    Window::cursorMode = mode;
    if(Window::window != nullptr) glfwSetInputMode(Window::window, GLFW_CURSOR, Window::cursorMode);
}

void Window::setWidth(const unsigned int newWidth){
    Window::width = newWidth;
    if(Window::window != nullptr) glfwSetWindowSize(Window::window, newWidth, Window::height);

}

void Window::setHeight(const unsigned int newHeight){
    Window::height = newHeight;
    if(Window::window != nullptr) glfwSetWindowSize(Window::window, Window::width, newHeight);
}

void Window::setXPos(const int newXPos){
    Window::xPos = newXPos;
    if(Window::window != nullptr) glfwSetWindowPos(Window::window, Window::xPos + (Window::monitor->getXPos()), Window::yPos);;
}

void Window::setYPos(const int newYPos){
    Window::yPos = newYPos;
    if(Window::window != nullptr) glfwSetWindowPos(Window::window, Window::xPos, Window::yPos + (Window::monitor->getYPos()));
}

void Window::setPos(const int newXPos, const int newYPos){
    Window::xPos = newXPos;
    Window::yPos = newYPos;
    if(Window::window != nullptr) glfwSetWindowPos(Window::window, Window::xPos + (Window::monitor->getXPos()), Window::yPos + (Window::monitor->getYPos()));
}

void Window::setSize(const unsigned int newWidth, const unsigned int newHeight){
    Window::width = newWidth;
    Window::height = newHeight;
    if(Window::window != nullptr) glfwSetWindowSize(Window::window, newWidth, newHeight);
}

void Window::setTitle(const std::string title){
    Window::name = title;
    if(Window::window != nullptr) glfwSetWindowTitle(Window::window, Window::name.c_str());
}

MWH::Monitor* Window::getMonitor(){
    if(Window::monitor == nullptr) throw MonitorError(MWH::MONITOR_NOT_INITIALIZED_ERROR, "Monitor has not been initialized so you can't get it at this time!");
    return Window::monitor;
}

void Window::setVSync(const bool newVSync){
    if(Window::context == Window::NO_CONTEXT || Window::context == Window::VULKAN_CONTEXT) throw WindowError(INVALID_CONTEXT_ERROR, "You can not use vsync with the context specified!");
    if(Window::window != nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "The window can't be set to use vsync after it's been created!");
    ///NOTE: Windows with no context can't have vsync as vsync is a context specific thing, and come contexts like vulkan are not really made to work with glfw plus you can set vsync using them, which for others is a bit harder to do ( *cough* *cough* opengl *cough* )
    Window::vsync = newVSync;
}

void Window::setFullScreen(const bool newFullscreen){
    Window::fullscreen = newFullscreen;

    if(Window::window != nullptr){
        if(Window::fullscreen)
            glfwSetWindowMonitor(Window::window, Window::monitor->mon, 0, 0, Window::monitor->getResolutionWidthInSC(), Window::monitor->getResolutionHeightInSC(), Window::monitor->getRefreshRateInHz());
        else
            glfwSetWindowMonitor(Window::window, nullptr, Window::xPos, Window::yPos, Window::width, Window::height, 0);
    }
}

void Window::setContextFocus(){
    if(Window::window == nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "Can't set context focus on a non-existant window!");
    if(Window::context != Window::OPENGL_CONTEXT) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "The context specified can't be focused on, because it's not supported greatly!");
    glfwMakeContextCurrent(Window::window);
}

void Window::setInputFocus(){
    if(Window::window == nullptr) throw WindowError(WINDOW_FUNCTION_NOT_AVAILABLE_ERROR, "Can't set input focus on a non-existant window!");
    glfwFocusWindow(Window::window);
}

const bool Window::isKeyPressed(const int keyCode){
    if(Window::window == nullptr) throw WindowError(MWH::WINDOW_NOT_CREATED_ERROR, "You must create the window before trying to get the keybaord key that is currently pressed!");
    return (glfwGetKey(Window::window, keyCode) == GLFW_PRESS);

}

const bool Window::isMouseKeyPressed(const int keyCode){
    if(Window::window == nullptr) throw WindowError(MWH::WINDOW_NOT_CREATED_ERROR, "You must create the window before trying to get the mosue key that is currently pressed!");
    return (glfwGetMouseButton(Window::window, keyCode) == GLFW_PRESS);
}


