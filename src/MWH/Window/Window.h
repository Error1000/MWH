#pragma once

#include "../API/API.h"
#include "../Monitor/Monitor.h"
#include "../Monitor/Monitors.h"
#include "WindowError.h"
#include "Image.h"
#include "Cursor.h"

/**
    @file Window.h

    @brief Contains definitions of Window related things
*/



namespace MWH{


/**
    @brief An object that handles the window
*/
class Window{
    public:

    /**
        @enum Window::Context

        @brief Contains a list of window contexts/renderers that can be attached to the window
    */
    enum Context : unsigned short{
     OPENGL_CONTEXT = 0, VULKAN_CONTEXT = 1, NO_CONTEXT = 2
    };

    private:

    // Variables
    int width = 400;
    int height = 400;

    int xPos = 0;
    int yPos = 0;

    double mouseXPos = 0;
    double mouseYPos = 0;
    bool cursorInside = false;

    std::string name = "Window";
    bool fullscreen = false;
    GLFWwindow* window = nullptr;
    MWH::Monitor* monitor = nullptr;
    Window::Context context;
    bool resizeable = false;
    bool hidden = false;
    bool minimised = false;
    bool maximised = false;

    // Callbacks
    void (*onWindowResizeFunc)(MWH::Window* win) = nullptr;
    void (*onWindowPosChangeFunc)(MWH::Window* win) = nullptr;
    void (*onFileOrFolderDroppedOnWindow)(MWH::Window* win, int count, std::string* paths) = nullptr;
    void (*onCursorPosChangeFunc)(MWH::Window* win) = nullptr;
    void (*onKeyboardKeyChangeFunc)(MWH::Window* win, int key, int keyScanCode, int action, int modifiers) = nullptr;
    void (*onMouseKeyChangeFunc)(MWH::Window* win, int key, int action, int modifiers) = nullptr;
    void (*onCursorEnterOrLeaveFunc)(MWH::Window* win) = nullptr;

    void* userPointer = nullptr;
    MWH::Window* toShareWith = nullptr;

    // Icon
    int iconCount = 0;
    MWH::Image* icons = nullptr;

    //Cursor
    MWH::Cursor* cursor = nullptr;
    MWH::Cursor::Mode cursorMode = MWH::Cursor::CURSOR_NORMAL;

    // FPS / frame tools
    double lastSecond = 0;
    double lastTime = 0;
    unsigned int fps = 0;
    unsigned int nbFrames = 0;
    bool vsync = false;

    double deltaTime = 0;
    double totalDeltas = 0;
    unsigned int deltas = 0;


   public:

    // Function declarations

    inline explicit Window(MWH::Monitor* mon, const Window::Context con = Window::NO_CONTEXT)
    try:
    // Initialize variables that can't be initialized globally
    // The monitor initialization is this way since the obejct returned by the function is temporary so we can't take it's address, so we make our own new pointer object and copy the temporary into the new pointer, which then gets copied to our pointer
    monitor(mon), lastSecond(API::getCurrentTime()), lastTime(API::getCurrentTime())
    { Window::setContext(con); }
    catch(const std::exception& e){
        if(!API::isInitialised())
            throw WindowError(WINDOW_CREATION_ERROR, "Can't create window object if the API is not initialised!");
        else
            throw e;
    }

    inline explicit Window(const Window::Context con = Window::NO_CONTEXT)
    try:
    // Initialize variables that can't be initialized globally
    // The monitor initialization is this way since the obejct returned by the function is temporary so we can't take it's address, so we make our own new pointer object and copy the temporary into the new pointer, which then gets copied to our pointer
    monitor(new MWH::Monitor(MWH::API::getPrimaryMonitor())), lastSecond(API::getCurrentTime()), lastTime(API::getCurrentTime())
    { Window::setContext(con); }
    catch(const std::exception& e){
        if(!API::isInitialised())
            throw WindowError(WINDOW_CREATION_ERROR, "Can't create window object if the API is not initialised!");
        else
            throw e;
    }


    // Destroy window if object destructor gets called
    inline ~Window(){
        if(Window::window != nullptr && MWH::API::isInitialised()) destroyWindow();
        delete Window::monitor;
        // Note: Deleting a function pointer dosen't work and isn't needed because that would mean deleting the function from existence, and we don't need that, so we can just let the pointer go out of scope
    }

    /**
        @brief Creates a window based on the private variables set by public functions and gives it context focus

        @throw MWH::APIError Should only be thrown if the API is not initialized

        @throw MWH::WindowError Only happens if the window already exists or the API failed to create the window

        @warning Can not be called from a callback
    */
    void createWindow();


    /**
        @brief This function destroys the window

        @throw MWH::APIError Should only be thrown when the api has not been initialized yet or when a platform error occured

        @throw MWH::WindowError Only called if the window dosen't exist

        @warning This function only closes the window and resets mouse position and the fps counter

        @warning Can not be called from a callback
    */
    void destroyWindow();


    /**
        @brief This functions lets you share the context of the current window with another one

        @throw MWH::WindowError Should be called if the current window is created, the curretn window has no context or the contexts aren't the same

        @throw MWH::APIError Should only be thrown when the api has not been initialized yet
    */
    void shareContextWith(Window& other);


    /**
        @brief This function sets the context that the window will be created with

        @throw std::invalid_argument When the value passed as a context is not valid ( a null pointer or an unkown value or something else )

        @throw MWH::WindowError Is only thrown when the function is called after creating the window

        @throw MWH::APIError Should only be thrown when the api has not been initialized yet

    */
    void setContext(const Window::Context context);


    /**
        @brief This function updates most variables and things

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function

        @warning Should be called before using variables like mouse location, window postion, window size, etc.

        @warning Can not be called from a callback
    */
    inline void pollEvents(){ glfwPollEvents(); /* Poll new i/o events and manage callbacks */ }


    /**
        @brief This function updates the window ( draws the new frame, and updates the fps value )

        @throw MWH::WindowError Should only be thrown if the window dosen't exist

        @throw MWH::APIError Should only be thrown if the API is not initialized, the platform isn't supported, or the window dosen't have a context

        @warning Must only be called once per frame
    */
    void update();


    /**
        @brief Tell the os that the window should close

        @throw MWH::WindowError Should only be called if the window dosen't exist

        @throw MWH::APIError Should only be thrown if the API is not initialized
    */
    void setToClose(bool close = true);


    /**
        @brief Asks the os if the window should close

        @throw MWH::APIError Should only be thrown if the API is not initialized

        @throw MWH::WindowError Should only be called if the window dosen't exist

        @retval bool Wheter or nto the window should close
    */
    bool shouldClose();


    /**
        @brief Set the monitor the window starts on and uses

        @throw MWH::WindowError Should only be called if the window exists
    */
    void setMonitor(MWH::Monitor* m);

    /**
        @brief If users uses NO_CONTEXT/VULKAN_CONTEXT the ycan use this to createa a surface to draw on

    */
    // NOTE: This uses templates, teherefore it can't be forward declared and must be in-line
    // NOTE: Since this is not a alibrary tha deals with specific opengl/vulkan related things we basically avoid specific things as much as possible by using tempaltes so tas to make sure it's always going to be graphics api independant
    template<typename IT, typename ST>
    inline void createSurface(IT instance, ST* surface){
        if(Window::window == nullptr) throw WindowError(WINDOW_NOT_CREATED_ERROR, "Can't create a surface for the window if it dosen't exist!");
        if(glfwCreateWindowSurface(instance, Window::window, nullptr, surface)) throw std::runtime_error("Failed to create window surface!");
    }


    /**
        @brief Sets wheater or not the user can resize the window

        @warning This will not resize the context if the window is resized, you have to do that manually when the size gets changed

        @warning Can only be called if the window dosen't exist
    */
    void setResizeable(const bool resizeable);

    /**
        @brief Sets wheater or not the window is shown on screen
    */
    void setHidden(const bool hide);

    /**
        @brief Sets wheter or not the window is minimised
    */
    void setMinimised(const bool minimise);

    /**
        @brief Sets wheter or not the window is maximised
    */
    void setMaximised(const bool maximise);

    /**
        @brief Set the function that gets called whenever the window changes size

        @warning The callback only gets called if the Window::pollEvents() function is called
    */
    inline void setResizeCallback(void (*func)(MWH::Window* win)){ Window::onWindowResizeFunc = func; }


    /**
        @brief Set the function that gets called whenever a file or folder is dropped on the window

        @warning The callback only gets called if the Window::pollEvents() function is called
    */
    inline void setFileOrFolderDroppedCallback(void (*func)(MWH::Window* win, int count, std::string* paths)){ Window::onFileOrFolderDroppedOnWindow = func; }

    /**
        @brief Set the function that gets called whenever the window gets moved

        @warning The callback only gets called if the Window::pollEvents() function is called
    */
    inline void setPosChangeCallback(void (*func)(MWH::Window* win)){ Window::onWindowPosChangeFunc = func; }

    /**
        @brief Set the function that gets called whenever the cursor gets moved

        @warning The callback only gets called if the Window::pollEvents() function is called
    */
    inline void setCursorPosChangeCallback(void (*func)(MWH::Window* win)){ Window::onCursorPosChangeFunc = func; }

    /**
        @brief Set the function that gets called whenever a new event is recived from the keyboard

        @warning The callback only detects events from the keyboard if the window has input focus

        @warning The callback only gets called if the WMH::Window::pollEvents() function is called
    */
    inline void setKeyboardKeyChangeCallback(void (*func)(MWH::Window* win, int key, int keyScanCode, int action, int modifiers)){ Window::onKeyboardKeyChangeFunc = func; }

    /**
        @brief Set the function that gets called whenever the cursor gets moved

        @warning The callback only detects events from the keyboard if the window has input focus

        @warning The callback only gets called if the Window::pollEvents() function is called
    */
    inline void setMouseKeyChangeCallback(void (*func)(MWH::Window* win, int key, int action, int modifiers)){ Window::onMouseKeyChangeFunc = func; }


    /**
        @brief Set the function that gets called whenever the cursor enters or leaves the window

        @warning The callback only gets called if the Window::pollEvents() function is called
    */
    inline void setCursorEnterOrLeaveCallback(void (*func)(MWH::Window* win)){ Window::onCursorEnterOrLeaveFunc = func; }

    /**
        @brief Used to associate an object the user specifies to a window object
    */
    inline void setUserPointer(void* p){ Window::userPointer = p; }

    /**
        @brief Sets the set of icons the window should use ( the system chooses the best resolution on its own ), good sizes are 16x16, 32x32 and 48x48

        @note If you don't specify the count it will assume that a one element array has been passed, or that just a normal pointer that is not an array has been passed
    */
    void setIcon(MWH::Image* newIcons, int c = 1);

    /**
        @brief Sets a custom cursor the window should use
    */
    void setCursor(MWH::Cursor* newCursor);

    /**
        @brief Sets the mode of tthe cursor in this window
    */
    void setCursorMode(MWH::Cursor::Mode mode);

    /**
        @brief Set the height of the window in Screen Coordinates ( SC ) ( usually pixels )

        @warning This function will not resize the context, this has to be doen manually in for example, a callback such as the window size change one

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function
    */
    void setHeight(const unsigned int height);


    /**
        @brief Sets the width of the window in Screen Coordinates ( SC ) ( usually pixels )

        @warning This function will not resize the context, this has to be doen manually in for example, a callback such as the window size change one

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function
    */
    void setWidth(const unsigned int width);


    /**
        @brief Sets the width and height of the window in Screen Coordinates ( SC ) ( usually pixels )

        @warning This function will not resize the context, this has to be doen manually in for example, a callback such as the window size change one

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function
    */
    void setSize(const unsigned int width, const unsigned int height);


    /**
        @brief Sets the x position of the window on the current screen, (0,0) being the top left corner of the screen each unit being a pixel

        @warning Window snaping by the os still applies to this function, so setting the window location to (0, 0) might not yield in a position change that has as coordinates (0, 0)!

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function
    */
    void setXPos(const int newXPos);


    /**
        @brief Sets the y position of the window on the current screen, (0,0) being the top left corner of the screen

        @warning Window snaping by the os still applies to this function, so setting the window location to (0, 0) might not yield in a position change that has as coordinates (0, 0)!

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function
    */
    void setYPos(const int newYPos);


    /**
        @brief Sets the x and y position of the window on the current screen, (0,0) being the top left corner of the screen each unit being a pixel

        @warning Window snaping by the os still applies to this function, so setting the window location to (0, 0) might not yield in a position change that has as coordinates (0, 0)!

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function
    */
    void setPos(const int newXPos, const int newYPos);


    /**
        @brief Return the width of the window in Screen Coordinates ( SC ) ( usually pixels )

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval int the width of the window
    */
    inline const int getWidth(){ return Window::width; }


    /**
        @brief Return the height of the window in Screen Coordinates ( SC ) ( usually pixels )

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval int the height of the window
    */
    inline const int getHeight(){ return Window::height; }


    /**
        @brief Return the x position of the window, each unit being a pixel

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval int the x position of the window
    */
    inline const int getXPos(){ return Window::xPos; }


    /**
        @brief Return the y position of the window, each unit being a pixel

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval int the y position of the window
    */
    inline const int getYPos(){ return Window::yPos; }

    /**
        @brief Get the current cursors of the window

        @retval MWH::Cursor* The current cursor of the window
    */
    inline MWH::Cursor* getCursor(){ return Window::cursor; }


    /**
        @brief Sets the title of the window

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform dosen't support this function
    */
    void setTitle(const std::string title);


    /**
        @brief Returns the title of the window

        @retval std::string The name of the window
    */
    inline const std::string getTitle(){ return Window::name; }


    /**
        @brief Returns a pointer to the monitor the window is on

        @throw MWH::MonitorError Should only be thrown if the monitor hasn't been initialized yet

        @retval MWH::Monitor* the monitor pointer
    */
    MWH::Monitor* getMonitor();


    /**
        @brief Set wheater or not the window should use vsync

        @throw MWH::WindowError Should only be thrown if the function is called after creating the window or the context dosen't support vsync with the API

        @warning Should only be called if the window dosen't exist
    */
    void setVSync(const bool vsync);


    /**
        @brief Set wheater or not the window is fullscreen

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform isn't supported
    */
    void setFullScreen(const bool fullscreen);

    /**
        @brief Set the focused context ( where stuff will be drawn ) to be this window's context

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform isn't supported
    */
    void setContextFocus();

    /**
        @brief Steal input focus

        @throw MWH::APIError Should only be thrown if the API is not initialized or the platform isn't supported
    */
    void setInputFocus();

    /**
        @brief Returns the average delta of all the moments between frames recorded

        @retval double the average time
    */
    inline const double getAverageDelta(){ return totalDeltas / deltas; }

    /**
        @brief Returns the delta of the moments between the last two frames

        @retval double the time
    */
    inline const double getDelta(){ return Window::deltaTime; }



    // TODO: Make these use chached values from callbacks instead of calling a getter function each time
    /**
        @brief Checks wheater or not the keyboard key with the key code specified is pressed or not

        @throw MWH::WindowError Should only be thrown if the window hasn't been created yet

        @throw MWH::APIError Should only be thrown if the API has not been initialized yet

        @throw std::invalid_argument Should only be thrown if the key code is not valid

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval bool Wheater or not the keyboard key is pressed
    */
    const bool isKeyPressed(const int keyCode);


    /**
        @brief Checks wheater or not the mouse key with the key code specified is pressed or not

        @throw MWH::WindowError Should only be thrown if the window hasn't been created yet

        @throw MWH::APIError Should only be thrown if the API has not been initialized yet

        @throw std::invalid_argument Should only be thrown if the key code is not valid

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval bool Wheater or not the mouse key is pressed
    */
    const bool isMouseKeyPressed(const int keyCode);

    /**
        @brief Returns wheter or not the cursor is inside the window or not

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval bool Wheter or not the cursor is inside the window or not
    */
    const bool isCursorInside(){ return Window::cursorInside; }

    /**
        @brief Return wheter ot not the window is resizeable by the user

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval bool Wheter or not it's resizeable
    */
    const bool isResizeable(){ return Window::resizeable; }

    /**
        @brief Return wheter ot not the window is hidden

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval bool Wheter or not it's hidden
    */
    const bool isHidden(){ return Window::hidden; }


    /**
        @brief Return wheter ot not the window is minimised or iconified

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval bool Wheter or not it's minimised / iconified
    */
    const bool isMinimised(){ return Window::minimised; }

    /**
        @brief Returns the x pos of the mouse in Screen Coordinates ( SC ) ( usually pixels )

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval double The x position
    */
    inline const double getMouseXPos(){ return Window::mouseXPos; }


    /**
        @brief Returns the y pos of the mouse in Screen Coordinates ( SC ) ( usually pixels )

        @warning Dosen't get updated if Window::pollEvents() isn't called

        @retval double The y position
    */
    inline const double getMouseYPos(){ return Window::mouseYPos; }


    /**
        @brief Returns the calculated fps

        @warning FPS is calulated in the MWH::Window::update() function so if that is not called the fps remains at 0

        @retval int the fps
    */
    inline const int getFPS(){ return Window::fps; }

    /**
        @brief Get user pointer
    */
    inline void* getUserPointer(){ return Window::userPointer; }
};

}

