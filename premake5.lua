workspace "MWH"
   architecture "x64"
   configurations { "DebugOpenGL", "DebugVulkan", "Release" }

outputDir = "%{cfg.buildcfg}"
outputFileName = "%{prj.name}%{cfg.buildcfg}-%{cfg.system}.%{cfg.architecture}"

project "MWH"
   language "C++"
   targetdir ( "bin/" .. outputDir )
   objdir ("obj/" .. outputDir)
   targetname ( outputFileName )
   cppdialect "c++17"
   
   -- No, the /*/* is not a typo
   files { "src/%{prj.name}/*/*.h", "src/%{prj.name}/*/*.cpp" }
   -- Linux specific
   filter "system:linux"
	links { "glfw" }
   filter ( "system:linux", "configurations:DebugOpenGL" )
	links { "GLEW", "GL" }
   filter ( "system:linux", "configurations:DebugVulkan" )
	links { "vulkan" }

   filter "system:macos"
	includedirs { "/usr/local/include" }
	libdirs { "/usr/loca/lib" }
	links { "glfw" }
   filter ( "system:macos", "configurations:DebugOpenGL" )
	links { "glew" }
	premake.tools.gcc.libraryDirectories.architecture.x86_64 = { "-framework OpenGL" }
   filter ( "system:linux", "configurations:DebugVulkan" )
	-- Not supported, because macos currently dosen't really support vulkan

   filter "system:windows"
	includedirs { "Libraries/windows/glfw-*/include" }
	libdirs { "Libraries/windows/glfw-*/lib-mingw-w64" }
	links { "glfw3" , "gdi32" }
   filter ( "system:windows", "configurations:DebugOpenGL" )
	includedirs { "Libraries/windows/glew-*/include" }
	libdirs { "Libraries/windows/glew-*/lib/Release/x64" }
	links { "glew32s", "opengl32" }
   filter ( "system:windows", "configurations:DebugVulkan" )
	-- Not supported, because i can't figure out how to use vulkan on windows

   filter "configurations:DebugOpenGL"
      kind "ConsoleApp"
      symbols "On"
      files {"src/%{prj.name}/OpenGLTest.cpp"}
   
   filter "configurations:DebugVulkan"
      kind "ConsoleApp"
      symbols "On"
      files {"src/%{prj.name}/VulkanTest.cpp"}

   filter "configurations:Release"
      kind "StaticLib"
      symbols "Off"
      optimize "On"